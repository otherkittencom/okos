"use strict"
var pkg = require("./package.json")
var debug = require('debug')(pkg.name)
var path = require('path')
var os = require("os")

var soundFile = "./api/res/Meow-sound-effect.mp3"
if (os.platform() == "darwin") {
  var spawn = require('child_process').spawn,
    afplay = spawn("afplay", [soundFile])

  afplay.on("data", function (data) {
    console.log("Data: " + data)
  })

  afplay.on("error", function (error) {
    console.log("Error: " + error)
  })

  afplay.on('close', function (code) {
    console.log('child process exited with code ' + code)
  })

} else if (os.platform() == "linux") {
  var spawn = require('child_process').spawn,
    mpg123 = spawn("mpg123", [soundFile])

  mpg123.on("data", function (data) {
    console.log("Data: " + data)
  })

  mpg123.on("error", function (error) {
    console.log("Error: " + error)
  })

  mpg123.on('close', function (code) {
    console.log('child process exited with code ' + code)
  })
} else {
  console.log("os = " + os.platform())
}