"use strict"
var lambdaFactory = require("./lib/lambdaFactory.js")
var echo = require("./api/echo.js")
var meow = require("./api/meow.js")
var express = require("express")
var bodyParser = require("body-parser")
var util = require("util")
var app = express()

//create application/json parser
app.use(bodyParser.json()) //for parsing application/json
app.use(bodyParser.urlencoded({
    extended: false
  })) //for parsing application/x-www-form-urlencoded

var multer = require('multer') // v1.0.5
var upload = multer() // for parsing multipart/form-data

//Route the appropriate request methods (OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT, PATCH)

//All GET Requests
app.use(express.static("homepage")) //Home page handles all GET requests

//echo - POST
app.post('/echo', function (req, res) {
  //Check for header

  //console.dir(req.body);
  //res.send("test");

  if (req.body) {
    //console.log("" + req.body)
    //console.log(util.inspect(req.body))
    res.setHeader("Content-Type", "application/json")
    echo.handler(req.body, lambdaFactory.getContext(function succeed(result) {
      res.send(result)
    }, function fail(error) {
      res.send(error)
    }))
  } else {
    return res.sendStatus(400)
  }
})

//meow - POST
app.post('/meow', function (req, res) {
  if (req.body) {
    res.setHeader("Content-Type", "application/json")
    meow.handler(req.body, lambdaFactory.getContext(function succeed(result) {
      res.send(result)
    }, function fail(error) {
      res.send(error)
    }))
  } else {
    return res.sendStatus(400)
  }
})



var port = 3000
var server = app.listen(port, function () {
  console.log("okOS listening port: " + port)
})