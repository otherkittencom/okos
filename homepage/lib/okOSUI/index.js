var okOSUI = {}

okOSUI.initialize = function initialize() {
    okOSUI.initializeEcho()
    okOSUI.initializeMeow()
}

okOSUI.initializeEcho = function initializeEcho() {
  var echoButton = document.getElementById("echoButton")
  if (echoButton) {
    echoButton.addEventListener("click", echoButtonClickListener)
  }
}

function echoButtonClickListener() {
  var serviceURL = "/echo"
  var echoRequestBody = {}
  echoRequestBody.text = document.getElementById("echoText").value
  //echoRequestBody.version = 3.01
  //echoRequestBody.username = "bill"

  var echoRequest = new XMLHttpRequest()

  echoRequest.onreadystatechange = function () {
    if (echoRequest.readyState == httpReadyState.complete) {
      if (echoRequest.status == 200) {
        console.log("Response Type: " + echoRequest.responseType)
        var echoResponseText = document.getElementById("echoResponseText")
        echoResponseText.innerText = echoRequest.response
      }
      else {
        console.log("request failed: " + echoRequest.status)
        echoResponseText.innerText = "ERROR: " + echoRequest.status
      }
    }
  }

  echoRequest.open('POST', serviceURL, true)
  echoRequest.setRequestHeader("Content-Type", "application/json")
  echoRequest.send(JSON.stringify(echoRequestBody))
}

okOSUI.initializeMeow = function initializeMeow() {
  var meowButton = document.getElementById("meowButton")
  if (meowButton) {
    meowButton.addEventListener("click", meowButtonClickListener)
  }
}

function meowButtonClickListener() {
  var serviceURL = "/meow"
  var meowRequestBody = {}
  meowRequestBody.text = "mew"
  //echoRequestBody.version = 3.01
  //echoRequestBody.username = "bill"

  var meowRequest = new XMLHttpRequest()

  meowRequest.onreadystatechange = function () {
    if (meowRequest.readyState == httpReadyState.complete) {
      if (meowRequest.status == 200) {
        console.log("Response Type: " + meowRequest.responseType)
        var meowResponseText = document.getElementById("meowResponseText")
        meowResponseText.innerText = meowRequest.response
      }
      else {
        console.log("request failed: " + meowRequest.status)
        echoResponseText.innerText = "ERROR: " + meowRequest.status
      }
    }
  }

  meowRequest.open('POST', serviceURL, true)
  meowRequest.setRequestHeader("Content-Type", "application/json")
  meowRequest.send(JSON.stringify(meowRequestBody))
}