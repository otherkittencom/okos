var httpReadyState = {
  uninitialized: 0,
  loading: 1,
  loaded: 2,
  interactive: 3,
  complete: 4
}

document.onreadystatechange = function () {
  if (document.readyState == "interactive") {
    displayNavBar()
    displayokOSUI()
    displayFooter()
  }
}

function displayNavBar() {
  var navbarElement = document.getElementById("navbar")
  if (navbarElement) {
    $.get("/html/navbar.html", function (data) {
      navbarElement.innerHTML = data
    })
  }
}

function displayokOSUI() {
  var okOSUIElement = document.getElementById("okOSUI")
  if (okOSUIElement) {
    $.get("/lib/okOSUI/index.html", function (data) {
      okOSUIElement.innerHTML = data
      okOSUI.initialize()
      okOSUI.initializeEcho()
    })
  }
}

function displayFooter() {
  var footerElement = document.getElementById("footer")
  if (footerElement) {
    $.get("/html/footer.html", function (data) {
      footerElement.innerHTML = data
    })
  }
}