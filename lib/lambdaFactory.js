exports.getContext = function getContext(succeed, fail) {
  var context = {}

  context.succeed = function (result) {
    succeed(result)
  }

  context.fail = function (error) {
    fail(error)
  }

  context.done = function (error, result) {
    if (error) {
      fail(error)
    } else {
      succeed(result)
    }
  }

  return context
}