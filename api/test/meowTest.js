"use strict"

const assert = require("assert")
const util = require("util")

const meow = require("../meow.js")

describe("Insure Meow works properly", meowTest)

function meowTest() {
  it("Returns a profile", function (done) {
    this.timeout(6000)
    var context = {}

    context.succeed = function (result) {
      done()
    }

    context.fail = function (error) {
      done(error)
    }

    context.done = function (error, result) {
      done()

    }

    var event = null
    meow.handler(event, context)
  })
}