"use strict"

const assert = require("assert")
const util = require("util")

const echo = require("../echo.js")

describe("Insure Echo works properly", echoTest)

function echoTest() {
  it("Returns whatever is sent", function (done) {

    var context = {}

    context.succeed = function (result) {
      done()
    }

    context.fail = function (error) {
      done(error)
    }

    context.done = function (error, result) {
      done()

    }

    var message = "sent from home page"
    var event = message
    echo.handler(event, context)
  })
}