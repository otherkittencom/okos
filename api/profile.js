"use strict"

exports.profile = profile

/**
 * Returns the profile information for the Other Kitten Hardware
 * @param {Number} a
 * @param {Number} b
 * @returns {profileSchema}
 */
function profile() {
  var profile = {}
  profile.name = ""
  profile.ipAddress = ""
  profile.port = ""
  profile.hardware = ""
  profile.os = ""
  return profile
}

var os = require("os")

function getProfileSchema() {
  var profileSchema = {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "name": "The Machine name of the Other Kitten",
    "ipAddress": "The Other Kitten ipAddress",
    "port": "ip port",
    "hardware": "The hardware the Other Kitten is running on",
    "os": "The OS the Other Kitten is running on",
    "type": "object"
  }
  return profileSchema
}