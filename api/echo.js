"use strict"

function echo(message) {
  return message
}

exports.handler = function (event, context) {
  var echoResponse = echo(event)
  context.succeed(echoResponse)
}