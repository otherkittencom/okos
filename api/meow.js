"use strict"
var pkg = require("../package.json")
var debug = require('debug')("meow")
var path = require('path')
var os = require("os")

var soundFile = "./api/res/Meow-sound-effect.mp3"

function meow(context) {
  if (os.platform() == "darwin") {
    var spawn = require('child_process').spawn,
      afplay = spawn("afplay", [soundFile])

    afplay.on("data", function (data) {})

    afplay.on("error", function (error) {
      context.fail(error)
    })

    afplay.on('close', function (code) {
      context.succeed("Meowed")
    })
  } else if (os.platform() == "linux") {
    var spawn = require('child_process').spawn,
      mpg123 = spawn("mpg123", [soundFile])

    mpg123.on("data", function (data) {})

    mpg123.on("error", function (error) {
      context.fail(error)
    })

    mpg123.on('close', function (code) {
      context.succeed("Meowed")
    })


  } else {
    console.log(os.platform)
  }
}

exports.handler = function (event, context) {
  meow(context)
}