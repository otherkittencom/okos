echo "testing echo"
echo "simple body"
curl --data "username=bill" localhost:3000/echo
echo ""
echo "json body"
curl --data "{'username':'bill'}" localhost:3000/echo
echo ""
echo "json body with header"
jsondata='{"text":"Hello from OKOS Web Page","version":3.01,"username":"bill"}'
curl -X POST --header "Content-Type: application/json" --data "$jsondata" localhost:3000/echo
echo ""
echo "--finished testing echo"
