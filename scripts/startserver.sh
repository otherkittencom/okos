if [ "$#" == "1" ]; then
  httpPort=$1
else
    httpPort=8455
    echo "port not specified, starting server on default port"  
fi

echo "Serving the home page from port: $httpPort"
python -m SimpleHTTPServer $httpPort &