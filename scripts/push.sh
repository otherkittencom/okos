#!/bin/bash
scripts/integration.sh
timestamp=$(date '+%m%d%Y%H%M');
filename="okOS_$timestamp.zip"
builddirectoryname="okOS"
distdirectoryname="dist"
echo "creating $filename from $builddirectoryname"
#aws s3 ls s3://prod1.otherkitten.com/dist/
rm -R $builddirectoryname
mkdir $builddirectoryname
cp index.js $builddirectoryname
cp package.json $builddirectoryname
cp okMeow.js $builddirectoryname
cp README.md $builddirectoryname
cp -R api $builddirectoryname
cp -R lib $builddirectoryname
cp -R homepage $builddirectoryname
zip -r $distdirectoryname/$filename $builddirectoryname
rm -R $builddirectoryname
aws s3 sync $distdirectoryname s3://prod1.otherkitten.com/dist
