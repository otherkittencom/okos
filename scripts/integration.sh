JSPath="homepage/js/*.js index.js okMeow.js api/*.js api/test/*.js lib/*.js"
JSUnitTestPath="api/test"
HTMLPath="homepage/index.html homepage/html/*.html homepage/lib/okOSUI/index.html"
CSSPath="homepage/css/*.css homepage/lib/okOSUI/index.css"

#javascript beautifier
js-beautify $JSPath -r --good-stuff --indent-size 2
html-beautify $HTMLPath -r --indent-inner-html
css-beautify $CSSPath -r 
#javascript code style checker
jscs $JSPath
#TODO: IF jscs fails then end

#javascript unit tests
mocha --harmony $JSUnitTestPath

#TODO
#javascript code coverage

#TODO
#javascript documentation

#TODO
#html beautify, code style checker

#TODO
#css beautify, code style checker
